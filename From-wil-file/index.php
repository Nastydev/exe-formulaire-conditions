<?php
/*
Nous allons continuer sur l'envoi de fichier avec formulaires HTML + traitement en php ;
cette fois-ci nous allons aller plus loin sur le sujet de la vérification des fichiers,
et ensuite leur manipulation et surtout avec les images.
Je vous demande donc de faire un formulaire qui permettrait l'envoi d'images vers un dossier
 comme l'exercice de vendredi dernier mais en plus je vous demande de :

vérifier que le fichier est bien une image au format jpeg (sinon afficher un message d'erreur)
vérifier que le fichier fait moins de 1Mo (sinon afficher un message d'erreur)

vérifier que l'image a une taille d'au moins 800*800px (sinon afficher un message d'erreur)
ensuite, une fois tous les contrôles effectués, je vous demande de sauvegarder l'image comme la dernière fois mais de :

l'enregistrer avec un nom aléaoire pour éviter les conflits de noms de fichiers
créer une miniature (150px max) de l'image ayant le même nom mais enregistrée dans un autre dossier
ajouter un filigrane sur les images originales (pas sur les miniatures)
pour manipuler les images nous utiliserons la librairie GD (si elle n'est pas installée vous pouvez l'installer: sudo apt install php-gd :

doc globale de GD : https://www.php.net/manual/en/ref.image.php
fonction à utiliser pour redimentionner un fichier : https://www.php.net/manual/en/function.imagecopyresampled
à vous de faire un peu de recherche également pour trouver les fonctions qui peuvent vous être utilie dans GD et de revenir vers moi avec des questions, dites moi quand vous commencez à être bloqués que je vous fasse un live d'explications complémentaires
 */
//var_dump($_FILES);
if (isset($_FILES['my_file'])) {
    if ($_FILES['my_file']['type'] === "image/jpeg") { // Check l'extension du fichier et autorise jpeg
        if ($_FILES['my_file']['size'] < 1000000) { // Check la taille maximum de l'image en ko
            /**     $size = getimagesize($_FILES["my_file"]["tmp_name"]); //Forme un tableau d'info avec les dimensions.
             * $width = $size[0];
             * $height = $size[1]; */
            list($width, $height) = getimagesize($_FILES["my_file"]["tmp_name"]);
            if ($width > "800" && $height > "800") {
                uploadImage($_FILES["my_file"]["tmp_name"], $width, $height); //ajout de Width et de Height a ma fonction
            } else {
                echo "image trop petite";
            }
        } else {
            echo "Fichier trop gros";
        }
    } else {
        echo "Type de fichier invalide";
    }
}

function uploadImage($imagepath, $imgWidth, $imgHeight)
{
    $fileName = rand(10000, 99999) . ".jpeg"; // nom du fichier qui va être upload,donc nom sauvegardé.
    move_uploaded_file($imagepath, "uploads/" . $fileName);
    $srcImage = imagecreatefromjpeg("uploads/" . $fileName); //image à retailler, l'image source!
    $ratio = $imgWidth / $imgHeight; // Calcul du ratio
    if ($ratio > 1) {
        $miniWidth = 150;
        $miniHeight = 150 / $ratio;
    } else {
        $miniWidth = 150 * $ratio;
        $miniHeight = 150;
    }
    $miniature = imagecreatetruecolor($miniWidth, $miniHeight); // définir la taille de la miniature
    imagecopyresampled(
        $miniature,
        $srcImage,
        0,
        0,
        0,
        0,
        $miniWidth,
        $miniHeight,
        $imgWidth,
        $imgHeight);
    /** Copy dans la miniature, image à retailler. Coordonnée de chaque images sources et a mettre
     * dans la miniature. et la taille de la miniature a prendre en compte. et récupération de la taille de l'image
     * d'origine, récupérer dans la première partie de code.
     */
    imagejpeg($miniature, "miniature/" . $fileName);
    /**  image a sauvegarder, où je veux la sauvegarder,
     * et le nom sous le quel la nouvelle image va être sauvegardé. */

    $waterMark = imagecreatefrompng("E1r9X.png"); //nom de l'image qui va être le filigramme.
    $waterMarkSize = getimagesize("E1r9X.png");
    imagecopyresampled(
        $srcImage,
        $waterMark,
        0,
        0,
        0,
        0,
        $imgWidth,
        $imgHeight,
        $waterMarkSize[0],
        $waterMarkSize[1]
    );
    /**image source, sur la quelle mettre le filigramme, l'image filligramme, les positions, les tailles. */

    imagejpeg($srcImage, "uploads/" .$fileName);

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form enctype="multipart/form-data" method="post">
    <input type="file" name="my_file" accept="image/jpeg,image/png">
    <input type="submit">
</form>

</body>
</html>


