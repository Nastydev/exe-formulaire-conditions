
<?php
$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["upload_image"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

//Vérification de la quantité en ko.
if ($_FILES["upload_image"]["size"] > 1000000) {
    echo "<p>" . "Image trop large" . "</p>";
    $uploadOk = 0;
}

//Vérification de l'extension.
if($imageFileType != "jpg" && $imageFileType != "jpeg") {
    echo "<p>" . "Seulement les JPG JPEG sont acceptés." . "</p>";
    $uploadOk = 0;
}

//Vérification de la taille hauteur/largeur de l'image.
    list($width, $height) = getimagesize($_FILES["upload_image"]["tmp_name"]);
    if ($width > "800" || $height > "800") {
        echo "<p>" ."Maixmum 800px par 800px". "</p>";
        $uploadOk = 0;
    }

// Si quantité, l'extension ou la taille est/sont touchés alors pas d'upload.
if ($uploadOk == 0) {
    echo "Désolé, votre image n'est pas upload.";
// si l'image est bien dans l'input "file" alors upload l'image en changeant le nom.
} else {
    if (move_uploaded_file($_FILES["upload_image"]["tmp_name"], "images/".time().".jpeg"))
    {
        echo "L'image a été upload.";
    } else {
        echo "Désolé, une erreur est survenu pendant l'upload.";
    }

}


?>
